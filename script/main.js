let button = $('.button');

$(document).ready(function(){
    $(".nav-ul").on("click","a", function(event){
        event.preventDefault();
        let id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({
            scrollTop: top
        }, 1000);
    });

    button.on('click', function(element){
        element.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    $(".header-bg-bt1").click(function(){
        $("#posts").slideToggle('slow');
        $(this).toggleClass('active');
    });
});

